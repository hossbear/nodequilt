# Herzlich Willkommen auf meiner Seite!
 
Quiltkult ist eine Seite für Liebhaberinnen und Freundinnen von Patchworkarbeiten, Applikationen, Quilts und kreativer Textilgestaltung.
 
Ich möchte allen Interessierten das Quilt- und Patchworknähen vorstellen, Ihnen die dazu notwendigen Techniken vermitteln und natürlich auch Ihre eigene Kreativität anregen.
 
Anregungen und leicht verständliche Anleitungen zum Nachnähen finden Sie auch in meinen, im Mai erschienenen Buch „[Hand Applikationen, eine Herzenssache](/buch "Mein erstes Buch - Hand Applikationen, eine Herzenssache - 2.Auflage Februar 2014")“.
 
Auf diesen Seiten können Sie mein Buch kaufen, Startersets bestellen, und später auch Quiltzubehör erwerben.
 
Viel Spaß beim Durchstöbern meiner Seiten!

![Gequiltete Blüte](/images/home/quiltbluete.png) ![Applizierte Herzen](/images/home/herzkissen.png) ![Applizierter Schmetterling](/images/home/schmetterling.png) 