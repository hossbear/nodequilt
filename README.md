# This is a real world example of [NodeJS](http://nodejs.org/ "Official Node JS Site") providing a full homepage

* The source code of this page can be found at [Bitbucket](https://bitbucket.org/hbehrendt/nodequilt)

### What is the above repository for? ###

* The repo contains all source code for the homepage of [QuiltKult](http://quiltkult.de) and therefore is intellectual property of (©) [QuiltKult](http://quiltkult.de) 2015 
* This code is published as sample and is NOT meant for unmodified reuse.

### License ###

* All Material is licensed under [Creative Commons Attribution 4.0 International Public License](http://creativecommons.org/licenses/by/4.0/)
* The Licensor revokes the right to reproduce and share the Licensed Material unmodified.
* The Licensor explicitly does not license any trademark used within this Licensed Material to you.

### How do I get set up? ###

You need to have [nodejs](http://nodejs.org/download/) and [git](http://git-scm.com/downloads) installed. 

The current application uses 0.10.35 but any version above this should also work.

Go to a directory you prefer and run:

1. `git clone https://hbehrendt@bitbucket.org/hbehrendt/nodequilt.git`
1. `cd nodequilt`
1.  `npm install`
1.  `node app.js`

and you are up an running

### Contribution guidelines ###

* No direct contribution please
* We are open for pull request 

### Who do I talk to? ###

* All this code is intellectual property of (©) [QuiltKult](http://quiltkult.de) 2015 and is administered by hossbear(at)planet.ms

### Some use full links
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)