/*
 * Module dependencies
 */
var log4js  = require('log4js')
  , express = require('express')
  , stylus = require('stylus')
  , morgan = require('morgan')
  , markdown = require('markdown-middleware')
  , cleanjson = require('node-json-minify')
  , fs = require('fs')
  ;

var port= 8888 ;
//process.env.PORT
/*
 * Module code
 */



/*
* configure logging
*/
log4js.configure(JSON.parse(cleanjson(fs.readFileSync('logging.json', "utf8"))), { cwd: '/var/log' });

var log = {
    con: log4js.getLogger('console')
  , req: log4js.getLogger('request')
  , app: log4js.getLogger('application')
}
//log.req.setLevel('ERROR')
/**
 * Setup morgan to use log4js
 */
var requestlogger = morgan(':status|:remote-addr|:remote-user|":method :url HTTP/:http-version"|:res[content-length]|":referrer"|":user-agent"',
  {
   stream: {
    write: function(str) { 
      
      switch(Number(str.substr(0,3))){
      case 200: log.req.trace(str);break;
      case 304: log.req.trace(str);break;

      case 100: log.req.info(str);break;
      case 101: log.req.info(str);break;
      case 102: log.req.info(str);break;
      case 201: log.req.info(str);break;
      case 202: log.req.info(str);break;
      case 203: log.req.info(str);break;
      case 204: log.req.info(str);break;
      case 205: log.req.info(str);break;
      case 206: log.req.info(str);break;
      case 207: log.req.info(str);break;
      case 208: log.req.info(str);break;
      case 226: log.req.info(str);break;
      case 300: log.req.info(str);break;
      case 301: log.req.info(str);break;
      case 302: log.req.info(str);break;
      case 303: log.req.info(str);break;
      case 305: log.req.info(str);break;
      case 306: log.req.info(str);break;
      case 307: log.req.info(str);break;
      case 308: log.req.info(str);break;
      default: log.req.error(str);
      }
    }
  }
});


/**
 *private compile for stylus
 */
function compile(str, path) {
  return stylus(str)
    .set('filename', path)
 //   .use(...)
}


/**
 * Setup the app
 */
var app = express();

app.locals.pretty = true;
app.locals.basedir =  __dirname + '/content';

app.enable('strict routing');

app.set('views', __dirname + '/views');

app.set('view engine', 'jade');

app.use(requestlogger);

app.use(markdown({directory: __dirname + '/content' }));

app.use(stylus.middleware(
  { src: __dirname + '/views'
  , compile: compile
  }
));

app.use(express.static(__dirname + '/public'));

/**
 * Set up the routes
 */
//Basic route for / , /index... , /home ...
app.get(new RegExp('^(\/$|\/(index|home)[^\/]*$)'), function (req, res) {
  res.render('index',{title:'Stich für Stich'});
  }
);
app.get(new RegExp('^(\/about[^\/]*$)'), function (req, res) {
  res.render('about',{title:'About this Site'});
  }
);

log.con.trace("application startup complete!");
log.app.trace("application startup complete!");
/**
 * Startup the app
 */
app.listen(port);
